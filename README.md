# Stop and Frisk Data

The FOIA responses for two Lucy Parsons Labs requests for six years of contact card data from 2012-17.

> Therefore, for the years between 2012 to 2014, we request all records in both the contact_cards and contact_traffic_stops tables. This means years 2012, 2013 and 2014. In case there are questions about the scope of the request, you can treat this request the same as FOIA P058306

- https://www.muckrock.com/foi/chicago-169/cpd-contact-cards-2014-2016-34241/
- https://www.muckrock.com/foi/chicago-169/cpd-contact-cards-2012-2014-38540/

For more details see, the [Lucy Parsons Lab Blog Post](https://lucyparsonslabs.com/posts/stop-and-frisk/)
